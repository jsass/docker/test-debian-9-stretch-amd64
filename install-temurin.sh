#!/bin/bash
set -eu

FEATURE_VERSION=11
OS=linux
ARCH=x64

while getopts 'f:o:a:?h' opt; do
  case "$opt" in
    f)
      FEATURE_VERSION=$OPTARG
      ;;

    o)
      OS=$OPTARG
      ;;

    a)
      ARCH=$OPTARG
      ;;

    ?|h)
      echo "Usage: $(basename $0) [-f FEATURE_VERSION] [-o OS] [-a ARCH]"
      exit 1
      ;;
  esac
done

cd "/opt"
echo "Installing Java $FEATURE_VERSION, OS $OS, Arch $ARCH"

# Specify the Java version and platform
API_URL="https://api.adoptium.net/v3/binary/latest/$FEATURE_VERSION/ga/$OS/$ARCH/jdk/hotspot/normal/eclipse"

# Fetch the archive
FETCH_URL=$(curl -s -w %{redirect_url} "${API_URL}")
FILENAME="${FETCH_URL##*/}"

echo "Fetching $FETCH_URL"
curl -OLs "${FETCH_URL}"

# Validate the checksum
curl -Ls "${FETCH_URL}.sha256.txt" | sha256sum -c --status

if [[ "$FILENAME" == *.zip ]]; then
  unzip -x "$FILENAME"

  DIRECTORY=$(unzip -Z1 "$FILENAME" | head -1)
  ln -snf "$DIRECTORY" "java$FEATURE_VERSION"

  rm "$FILENAME"
elif [[ "$FILENAME" == *.tar.gz ]]; then
  tar -xf "$FILENAME"

  DIRECTORY=$(tar tf "$FILENAME" | head -1)
  ln -snf "$DIRECTORY" "java$FEATURE_VERSION"

  rm "$FILENAME"
else
  echo "Unsupported archive $FILENAME"
  exit 1
fi

echo "installed"
